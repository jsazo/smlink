from setuptools import setup, find_packages
setup(
    name='smlink',
    author='Magnet S.P.A.',
    version='0.1.0',
    url='https://bitbucket.org/magnet-cl/smlink',
    description=('A Python tool to communicate with DLMS'
                 'compliant Smart Meters through Ethernet.'),
    packages=find_packages(),
    requires=['socket', 'multiprocessing'],
    install_requires=['socket', 'multiprocessing'],
    zip_safe=False
)
