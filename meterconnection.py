import errno
import logging
import socket
import sys

from multiprocessing import Process
from time import sleep

SOH = '\x01'
STX = '\x02'
ETX = '\x03'
ACK = '\x06'


logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s: %(message)s.',
                    filename='/tmp/smlink.log',
                    filemode='w')
shandler = logging.StreamHandler()
shandler.setLevel(logging.CRITICAL)
formatter = logging.Formatter('%(levelname)s: %(message)s.')
shandler.setFormatter(formatter)
logger.addHandler(shandler)


class SmartMeter(object):
    def __init__(self, host='', port='', smid='', passwd='00000000'):
        self.passwd = passwd
        self.smid = smid
        self.host = host
        self.port = port
        self.connection = Ethernet485(host=host) if host else Ethernet485()
        self.sock = None

    def __str__(self):
        return str(self.smid)

    def request_message(self):
        msg = '/?{}!\r\n'.format(self.smid)
        stop_str = '\r\n'
        logger.debug("sent: {}".format(msg))
        res = self.connection.send(msg, stop_str)
        if isinstance(res, int):
            return res
        logger.debug("received: {}".format(res))
        return 0

    def data_readout(self):
        self.connection.start()
        ret = self.request_message()
        if(ret != 0):
            return ret
        msg = '{}050\r\n'.format(ACK)
        logger.debug("sent: {}".format(msg))
        stop_str = '!\r\n{}'.format(ETX)
        res = self.connection.send(msg, stop_str)
        logger.debug("received: {}".format(res))
        self.connection.close()

    def programming_mode(self):
        ret = self.request_message()
        if(ret != 0):
            return ret
        sleep(0.3)
        logger.info("Entering programming mode")
        msg = '{}051\r\n'.format(ACK)
        logger.debug("sent: {}".format(msg))
        stop_str = ETX
        res = self.connection.send(msg, stop_str)
        if isinstance(res, int):
            return res
        self.smid = res[res.find("(")+1:res.find(")")]
        logger.debug("received: {}".format(res))
        # send password
        msg = '{}P1{}({}){}'.format(SOH, STX, self.passwd, ETX)
        BCC = self.bcc_sum(msg)
        msg += chr(BCC)
        stop_str = ACK
        logger.debug("sent: {}, BCC: {}".format(msg, BCC))
        ret = self.connection.send(msg, stop_str)
        logger.debug(ret)
        if ret == ACK:
            return 0
        return ret

    def read_location(self, address='', callback=None):
        ret = self.connection.start()
        if(ret != 0):
            return ret
        ret = self.programming_mode()
        if(ret != 0):
            return ret
        logger.info("Reading register")
        cmd = 'R5'
        msg = '{}{}{}{}(){}'.format(SOH, cmd, STX, address, ETX)
        BCC = self.bcc_sum(msg)
        msg += chr(BCC)
        logger.debug("sent: {}, BCC: {}".format(msg, BCC))
        stop_str = ETX
        res = self.connection.send(msg, stop_str)
        if not res:
            logger.error("Meter not reponding", exc_info=True)
            return errno.ENODATA
        elif isinstance(res, int):
            logger.error(res, exc_info=True)
            return res
        logger.debug("received: {}".format(res))
        #try:
        value = res[res.find("(")+1:res.find(")")]
        if value == "":
            logger.error('No data received', exc_info=True)
            self.break_cmd()
            self.connection.close()
            return 2
        value = float(res[res.find("(")+1:res.find("*")])
        unit = res[res.find("*")+1:res.find(")")]
        logger.info("value: {},  unit: {}".format(value, unit))
        response = {"value": value, "unit": unit}
        self.break_cmd()
        self.connection.close()

        if callable(callback):
            logger.debug("external call: {}({})".format(
                callback.__name__, response))
            try:
                callback(value)
            except:
                err = sys.exc_info()[0]
                logger.error("external call: {}({}). {}".format(
                    callback.__name__, response, err), exc_info=True)
                return 3
        else:
            return response

    def break_cmd(self):
        cmd = 'B0'
        msg = '{}{}{}'.format(SOH, cmd, ETX)
        self.connection.send(msg, '')

    @staticmethod
    def bcc_sum(msg):
        msg_array = []
        msg_array.extend(msg)
        msg_array = map(ord, msg_array)
        if SOH in msg:
            BCC = reduce(lambda x, y: x ^ y, msg_array[msg.index(SOH)+1:])
        elif STX in msg:
            BCC = reduce(lambda x, y: x ^ y, msg_array[msg.index(STX)+1:])
        else:
            BCC = reduce(lambda x, y: x ^ y, msg_array)
        return BCC


class Ethernet485(object):
    def __init__(self, host='192.168.1.111', port=5000):
        self.host = host
        self.port = port

    def __str__(self):
        return str(self.host)

    def start(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(3.0)
        try:
            self.sock.connect((self.host, self.port))
        except socket.error as serr:
            if serr.errno == errno.ECONNREFUSED:
                logger.error("Connection refused", exc_info=True)
                self.close()
                return errno.ECONNREFUSED
            else:
                logger.error("Unable to start connection", exc_info=True)
                return 1
        return 0

    def send(self, msg, stop_str=''):
        try:
            self.sock.send(msg)
        except socket.timeout:
            logger.error("Socket timeout error", exc_info=True)
            self.close()
            return errno.ETIMEDOUT
        except IOError, err:
            if err.errno == errno.EPIPE:
                logger.error("Communication pipe broken", exc_info=True)
                return errno.EPIPE

        if (stop_str == ''):
            return
        BUFFER_SIZE = 1
        try:
            data = self.sock.recv(BUFFER_SIZE)
        except socket.timeout:
            logger.error("Timeout error", exc_info=True)
            self.close()
            return errno.ETIMEDOUT
        except socket.error as serr:
            if serr.errno == errno.EBADF:
                logger.error("Bad file descriptor", exc_info=True)
                return errno.EBADF
        response = '' + data
        last_str = ' ' * (len(stop_str) - 1) + data
        if data in [STX, SOH]:
            BCC_check = True
            BCC = 0
        else:
            BCC_check = False
            BCC = ord(data)
        while(last_str != stop_str):
            try:
                data = self.sock.recv(BUFFER_SIZE)
            except socket.timeout:
                logger.error("Timeout error", exc_info=True)
                self.close()
                return errno.ETIMEDOUT
            last_str = last_str[1:] + data
            response += data
            if BCC_check:
                BCC ^= ord(data)
            if data in [STX, SOH]:
                BCC_check = True

        if BCC_check:
            BCC ^= ord(ETX)
            try:
                data = self.sock.recv(BUFFER_SIZE)
                response += data
                #print "BCC:", ord(data)
                #print 'Calculated BCC: ', BCC
            except socket.timeout:
                logger.error("Timeout error", exc_info=True)
                self.close()
                return errno.ETIMEDOUT
        return response

    def close(self):
        self.sock.close()


def get_active_demand(meter=SmartMeter(), callback=None):
    current = meter.read_location("31.7.0")
    meter.connection.close()
    if not isinstance(current, dict):
        logging.error("Error reading current. Aborting", exc_info=True)
        return current
    sleep(4)
    voltage = meter.read_location("32.7.0")
    if not isinstance(voltage, dict):
        logging.error("Error reading voltage. Aborting", exc_info=True)
        return voltage
    value = voltage["value"] * current["value"]
    power = {"value": value}
    if voltage["unit"] == 'V' and current["unit"] == 'A':
        power["unit"] = 'W'
    else:
        power["unit"] = ''

    if callable(callback):
        logger.debug("external call: {}({})".format(
            callback.__name__, power))
        try:
            callback(power)
        except:
            err = sys.exc_info()[0]
            logger.error("external call: {}({}). {}".format(
                callback.__name__, power, err), exc_info=True)
            return 3
    else:
        return power


def call_service(device_id=None, service_name='', callback=None):
    sm = SmartMeter(device_id)
    if(service_name == 'GET_CURRENT'):
        if callback:
            proc = Process(target=sm.read_location, args=("31.7.0", callback))
            proc.start()
        else:
            return sm.read_location("31.7.0")
    elif(service_name == 'GET_VOLTAGE'):
        if callback:
            proc = Process(target=sm.read_location, args=("32.7.0", callback))
            proc.start()
        else:
            return sm.read_location("32.7.0")
    elif(service_name == 'GET_ACTIVE_DEMAND'):
        if callback:
            proc = Process(target=get_active_demand, args=(sm, callback))
            proc.start()
        else:
            return get_active_demand(sm)
