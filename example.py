import meterconnection
import sys

from time import sleep

print('Blocking call to get meter voltage, current and power.')
voltage = meterconnection.call_service(device_id='192.168.1.111',
                                       service_name='GET_VOLTAGE')
# Object should be a dict with unit and value.
# If the object is an integer, an error ocurred.
if isinstance(voltage, int):
    print('Failed to get voltage')
    sys.exit(1)
else:
    print 'Voltage:', voltage

# A delay should be used to give the ethernet module time to respond
sleep(4)

current = meterconnection.call_service(device_id='192.168.1.111',
                                       service_name='GET_CURRENT')
if isinstance(current, int):
    print('Failed to get current')
    sys.exit(1)
else:
    print 'Current:', current

sleep(4)

demand = meterconnection.call_service(device_id='192.168.1.111',
                                      service_name='GET_ACTIVE_DEMAND')
if isinstance(demand, int):
    print('Failed to get demand')
    sys.exit(1)
print 'Active demand:', demand


# Callback method that writes to a file
def test_callback(arg):
    target = open('/tmp/smlink-example.tmp', 'w')
    target.write(str(arg))
    target.close()


sleep(4)
print('Non blocking call to get meter power.')
meterconnection.call_service(device_id='192.168.1.111',
                             service_name='GET_ACTIVE_DEMAND',
                             callback=test_callback)
print('If no error ocurred, after the power is being read the value')
print('would be written to /tmp/smlink-example.tmp')
